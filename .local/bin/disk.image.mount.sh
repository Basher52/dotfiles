#!/bin/env bash
#
#
#
#
#
#
#
#
info() {
  echo ""
  echo ""
  echo "Parameters into this script is"
  echo ""
  echo "1: disk image file including path"
  echo "2: path to mountpoint"
  echo "3: Optional, any character so image"
  echo "   is mount as writeable"
  echo ""
  echo "______________________________________________"
  echo "* This script has a maximum of 10 partitions *"
  echo ""
}

clear

if (( $EUID != 0 )); then
  echo ""
  echo -e $RED"This script needs root privileges\ndue to the 'mount' command"$Color_Off

  info
  exit
fi

if [ $# -eq 0 ]; then
  echo ""
  echo "No parameters set"
  info
  exit
fi

if [ $# -lt 2 ]; then
  echo ""
  echo "Script needs 2[3] paramters"
  info
  exit
fi

if ! [ -f "$1" ]; then
  echo ""
  echo "File not found"
  info
  exit
fi

imagefile=$1
mountpoint=$2
ro=${3:-"ro"} # if 3rd parm not set then set 'ro' as read only
# if 3rd parm is set to anything but ro then mount image as writeable
if [ "$ro" != "ro" ]; then
  ro=""
fi
nopart=10 # Number of partitions listed from the grep'd command below, can be set to more but 10 is quite enuf.

#set -x # Show trace for errors
# Check to see if mountpoint is already used
mountpoint $mountpoint > /dev/null
if [ $? -eq 0  ]; then
  echo ""
  echo "WARNING! "
  echo "WARNING! Mountpoint is in use"
  echo "WARNING! "
  echo ""
  echo -e "Do you wanna create a new unique directory for this? (Y/n)\n(This will be created under '/mnt/')"
  read ans
  ans=${ans:-"Y"} # Set default Y if nothing is entered
  if [ "${ans^^}" == "Y" ]; then
    mountpoint="/mnt/"$(mktemp -u XXXXXXXXXX)
    mkdir $mountpoint
  fi
fi
# Get 'sector size' of image but remove any errors/warnings
disksectorsize=$(fdisk -l $imagefile 2>/dev/null | awk '/Sector/ {print $4}' | head -n 1)

# Get number of partitions
noofpartitions=$(fdisk -l $imagefile 2>/dev/null | grep -A $nopart 'Device' | wc -l)
noofpartitions=$(calc $noofpartitions-1)

# Remove first line, this is to the number of lines 'grep' reads and is default 10
partitions=$(fdisk -l $imagefile 2>/dev/null | grep -A $nopart 'Device' | tail -n $noofpartitions)

# Get start sector
selpartsector=$(fdisk -l $imagefile 2>/dev/null | grep -A $nopart 'Device' | fzf | awk '{print $2}')

# Calculate offset in image file for selected partition
offset=$(calc $selpartsector*$disksectorsize)

#---------- ONLY USED TO CREATE NEW!! LOPP DEVICE -----------
# Create unique(almost) ID for the new loop device
#uid=$(echo $((1 + $RANDOM % 1000)))

# Set temporary loopback device
#loopdev="/dev/loop$uid"

# Create new loop-device
#create_loopdev=$(sudo losetup $loopdev $imagefile)
#---------- ONLY USED TO CREATE NEW!! LOPP DEVICE -----------

# Mount image
sudo mount -o $ro,loop,offset="$offset" $imagefile $mountpoint

exit 0

