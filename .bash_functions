#!/bin/env bash
#
#
# All useful functions are place here to be sourced
#
#
#-------------------------------------------------------------------------------
#   List all directories from current, max-depth=1, and their sizes
ov ()
{
  du -h --max-depth=1 | sed -r '
              $d; s/^([.0-9]+[KMGTPEZY]\t)\.\//\1/
      ' | sort -hr | column
}

#-------------------------------------------------------------------------------
#
myip ()
{
  ip -4 -o addr show scope global $(ip -4 route | grep default | head -n1 | awk '{print$5}') | awk '{gsub(/\/.*/, "",$4); print $4}' | head -n 1
}
