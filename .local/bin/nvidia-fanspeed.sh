#!/bin/env bash

fanspeed=$(nvidia-smi -q | grep 'Fan' | awk -F ': ' '/: /{print $2}' | awk -F ' %' '/ %/{print $1}')

bgcolor=""

if [[ $fanspeed -eq 0 ]]
then
  fancolor="#dddd00"
  bgcolor="bgcolor='#ff0000'"
elif [[ $fanspeed -lt 10 ]]
then
  fancolor="#ff9900"
elif [[ $fanspeed -lt 30 ]]
then
  fancolor="#bbbb00"
elif [[ $fanspeed -lt 40 ]]
then
  fancolor="#779900"
elif [[ $fanspeed -gt 40 ]]
then
  fancolor="#00ff00"
fi

#echo -e $colorcpu "PAC:$cpu $colorgpu AUR:$gpu" #ASCII colors
echo "<b><span $bgcolor color='$fancolor'>$fanspeed</span></b>" #pango markup colors for awesomewm


