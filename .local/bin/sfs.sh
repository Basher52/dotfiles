#!/bin/bash
#---------------------------------------------------------------------------------
#
# Script to use 'sshfs' to uppack file under selected folders into the
# configured diretory.
#
# Created by Oerjan 'Aliens' Snell.
#
# 211106  Script created, now just to mark one file at a time.
# 221001  Can now mark multiple directories to unpack the files beneath.
# 230629  Able via configuration to use different diretories.
#
# ================================================================================
# Copy- left,right,up,down to anyone that wanna use,modify,delete,ruin it.
# ================================================================================
#
#---------------------------------------------------------------------------------
if [[ $matching -eq 1 ]]; then
  extractdir="/games/_Temp-GW"
        exit 0
if

for arg
  do printf '%s\n' "$arg" >> /tmp/unrardirs
done

sort -o /tmp/unrardirs{,}

cat /tmp/unrardirs | while read line
do
  if compgen -G "$line/*.part01.rar" > /dev/null;
  then
    # unrar the file named *.part01.rar..part99.rar
    rar x "$line/*.part01.rar" $extractdir
  elif compgen -G "$line/*.rar" > /dev/null;
  then
    # unrar files named *.rar..r99
    rar x "$line/*.rar" $extractdir
  fi
done

rm /tmp/unrardirs

exit 0
