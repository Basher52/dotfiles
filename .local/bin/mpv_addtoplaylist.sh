#!/bin/bash
#---------------------------------------------------------------------------------
#
# Script to make a playlist from files marked in pcmanfm and if they are movies.
# Created by Oerjan 'Aliens' Snell.
#
# 211106  Script created, now just to mark one file at a time.
# 220228  Recreated it so you can do a multi-select and sort it.
# 230629  Added delete to the file first, so now no separate adds to it.
#
# ================================================================================
# Copy- left,right,up,down to anyone that wanna use,modify,delete,ruin it.
# ================================================================================
#
#---------------------------------------------------------------------------------
#echo $1 >> /tmp/mpv-playlist

rm /tmp/mpv-playlist

for arg
  do printf '%s\n' "$arg" >> /tmp/mpv-playlist
done

sort -o /tmp/mpv-playlist /tmp/mpv-playlist

exit 0
