#!/bin/env bash
#---------------------------------------------------------------------------------
#
# Script to list all BT-devices and with fzf select one
# then trust it and connect or disconnect.
#
# Created by Oerjan 'Aliens' Snell.
#
# 240105  Script created.
#
# ================================================================================
# Copy- left,right,up,down to anyone that wanna use,modify,delete,ruin it.
# ================================================================================
# Todo: Add code for when it won't connect due to 'connection refused' found in
#       'systemctl status bluetooth'
#       Can be done by remove it and pair it, trust and connect again.
#---------------------------------------------------------------------------------
selected_mac=$(bluetoothctl devices | fzf | awk '{print $2}')

bluetoothctl trust $selected_mac

ans=$(printf "connect\ndisconnect\n" | fzf)

bluetoothctl $ans $selected_mac

exit 0
