#/usr/bin/env bash
#
#
# Spindown the GameDisk HDD before removal.
# Before ripping the cord out this this:
# --------
# sync
# umount /point/point
# hdparm -y /dev/sdX
# --------
#
#=========================================================================
# Get space for HOME-dir
space_home=$(df -h /home/$USER | awk '{print $4}' | tail -n 1)
# Get space game disk... MUST!!! have a label called: GAMES
tmp=$(lsblk -o name,label,mountpoint | awk '$0~/GAMES/ {print $1,$3}')
# Get Device name from tmp contents
tmp_1=$(echo $tmp | awk '{print $1}')
# Get MountPoint
tmp_2=$(echo $tmp | awk '{print $2}')

# Set colors depending on contents
bgcol=""
if [[ "$tmp_1" == "" ]]; then
  # If device is empty then disk it not even connected to the computer
  space_game="NC"
  color2="#99dd00"
elif [[ "$tmp_2" == "" ]]; then
  # If no mountpoint is found at the correct place then the partition isn't mounted
  space_game="NM"
  color2="#dd8800"
  dev=$(echo $tmp_1 | cut -c7-)
  sudo mount /dev/$dev $HOME/Games/
else
  # Get the free space from the game-partition
  space_game=$(df -h $HOME/Games | awk '{print $4}' | tail -n 1)
  # Export this variable for later spindown and power off
  export GAMESDIR_DEVICE=$(df -h $HOME/Games | awk '{print $0}')
  tmp_color2=$(echo $space_game | rev | cut -c2- | rev)

  if [[ $tmp_color2 -lt 30 ]]
  then
    color2="#ff0000"
  elif [[ $tmp_color2 -lt 50 ]]
  then
    color2="#FFFFFF"
    bgcolor2="#ff9900"
    bgcol="bgcolor='$bgcolor2'>"
  elif [[ $tmp_color2 -lt 80 ]]
  then
    color2="#ccdd00"
  else
    color2="#00ff00"
  fi
fi
