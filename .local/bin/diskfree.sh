#/bin/env bash
#
#
#
#
#
#=========================================================================
#
# Get space for HOME-dir
space_home=$(df -h /home/$USER | awk '{print $4}' | tail -n 1)
# Get space game disk... MUST!!! have a label called: GAMES
tmp=$(lsblk -o name,label,mountpoint | awk '$0~/GAMES/ {print $1,$3}')
# Get Device name from tmp contents
tmp_1=$(echo $tmp | awk '{print $1}')
# Get MountPoint
tmp_2=$(echo $tmp | awk '{print $2}')

# Set colors depending on contents
bgcol=""
if [[ "$tmp_1" == "" ]]; then
  # If device is empty then disk it not even connected to the computer
  space_game="NC"
  color2="#99dd00"
elif [[ "$tmp_2" == "" ]]; then
  # If no mountpoint is found at the correct place then the partition isn't mounted
  space_game="NM"
  color2="#dd8800"
  dev=$(echo $tmp_1 | cut -c7-)
  sudo mount /dev/$dev $HOME/Games/
else
  # Get the free space from the game-partition
  space_game=$(df -h $HOME/Games | awk '{print $4}' | tail -n 1)
  # Export this variable for later spindown and power off
  export GAMESDIR_DEVICE=$(df -h $HOME/Games | awk '{print $1}')
  tmp_color2=$(echo $space_game | rev | cut -c2- | rev)

  if [[ $tmp_color2 -lt 30 ]]
  then
    color2="#ff0000"
  elif [[ $tmp_color2 -lt 50 ]]
  then
    color2="#FFFFFF"
    bgcolor2="#ff9900"
    bgcol="bgcolor='$bgcolor2'>"
  elif [[ $tmp_color2 -lt 80 ]]
  then
    color2="#ccdd00"
  else
    color2="#00ff00"
  fi
fi
# ------------------------------------------------------------------------

# Get space on the server that should be mount in the correct place, eg: /home/$USER/Gamteway
#space_server=$(df -h /home/$USER/Gateway | awk '$2~/fuse.sshfs/ {print $4, $7}')
space_server=$(df -h /home/$USER/Gateway | awk '{print $4}' | tail -n 1)
#tmp=$(lsblk -o name,label,mountpoint | awk '$2~/Gateway/ {print $1,$3}')
tmp=$(df -h /home/$USER/Gateway | awk '$6~/Gateway/ {print $5,$6}')
# Get Device name from tmp contents
tmp_1=$(echo $tmp | awk '{print $1}')
# Get MountPoint
tmp_2=$(echo $tmp | awk '{print $2}')

# Set colors depending on contents
if [[ "$tmp_1" == "" ]]; then
  # If device is empty then disk it not even connected to the computer
  space_server="NC"
  color3="#00ff00"
elif [[ "$tmp_2" == "" ]]; then
  # If no mountpoint is found at the correct place then the partition isn't mounted
  space_server="NM"
  color3="#dd8800"
else
  # Get the free space from the game-partition
  space_server=$(df -h $HOME/Gateway | awk '{print $4}' | tail -n 1)
  tmp_color3=$(echo $space_server | rev | cut -c2- | rev)

  if [[ $tmp_color3 -lt 500 ]]
  then
    color3="#ff0000"
  elif [[ $tmp_color3 -lt 800 ]]
  then
    color3="#ff9900"
  elif [[ $tmp_color3 -lt 900 ]]
  then
    color3="#99bb00"
  else
    color3="#00ff00"
  fi
fi

# ------------------------------------------------------------------------

tmp_color1=$(echo $space_home | rev | cut -c2- | rev)

if [[ $tmp_color1 -lt 10 ]]
then
  color1="#ff0000"
elif [[ $tmp_color1 -lt 20 ]]
then
  color1="#ff5500"
elif [[ $tmp_color1 -lt 30 ]]
then
  color1="#ff9900"
elif [[ $tmp_color1 -lt 40 ]]
then
  color1="#ffbb00"
elif [[ $tmp_color1 -lt 50 ]]
then
  color1="#aa9900"
elif [[ $tmp_color1 -lt 60 ]]
then
  color1="#99bb00"
else
  color1="#00ff00"
fi

echo "<b><span color='$color1'>$space_home</span><span color='#FFFFFF'>-</span><span color='$color2'$bgcol>$space_game</span><span color='#FFFFFF'>-</span><span color='$color3'>$space_server</span></b>"

exit 0
