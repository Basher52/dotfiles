#!/bin/bash
############################################################################
#	Script to help me git'ing and as I use an alias for git as 'bgit'
#	because I have a bare repo for my .dotfiles and such.
#	This will help out a lot for me.
############################################################################
bgitalias=" --git-dir=$HOME/.dotfiles/ --work-tree=$HOME "

git $bgitalias status ; echo -e "\n\n"

while true; do

  echo -e "0. bgit status\n1. bgit pull\n2. bgit checkout/retract\n3. bgit overwrite local file from remote\n4. bgit empty commit\n5. bgit push\n9. Exit"
  read ans

  case $ans in
    "0") # bgit status
      git $bgitalias status ; echo -e "\n"
      echo "Unpushed files:"
      files=`git $bgitalias diff-tree --name-only -r --no-commit-id HEAD`
      echo -e "        \033[0;31m$files\n\n\033[0m" ;;
    "1") # bgit pull
      git $bgitalias pull ; echo -e "\n\n" ;;
      #git $bgitalias pull ; echo "\n\n" ;;
    "2") # bgit checkout
      read -p "Gimme the filename to checkout/retract: " file
      git $bgitalias checkout $file ; echo -e "\n\n" ;;
    "3") # bgit overwrite local file from remote
      read -p "Gimme the filename to overwrite: " file
      git $bgitalias fetch
      git $bgitalias checkout origin/main $file ; echo -e "\n\n" ;;
    "4") # bgit empty commit
      git $bgitalias --allow-empty -m "Empty commit" ; echo -e "\n\n" ;;
    "5") # bgit push
      #/usr/bin/git $bgitalias push ssh://git@gitlab.com/Basher52/dotfiles.git ; echo -e "\n\n"
      /usr/bin/git $bgitalias push origin HEAD:main ; echo -e "\n\n"
      git $bgitalias status ; echo -e "\n\n" ;;
    "9") # exit
      exit ;;
    * )
      echo "Unknown parameter!" ; echo -e "\n\n" ;;
  esac
done

