#!/bin/env bash
#
# Start gpm to use a mouse in TTY (without x/wayland)
#

# If script run wituout x11, run script else exit
if [[ "*$DISPLAY*" == "**" ]];
then
  # Start gpm and save the PID for that process that will
  # be killed when X11/Wayland/? starts.
  gpm -m /dev/input/mouse0 -t ps2
  ps -A |grep -i gpm | awk '{print $1}' > /tmp/gpm_pid
fi
