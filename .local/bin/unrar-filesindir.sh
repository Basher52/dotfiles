#!/bin/bash
#---------------------------------------------------------------------------------
#
# Script to unrar all files under selected directories.
# Created by Oerjan 'Aliens' Snell.
#
# 230601  Script created.
#
# ================================================================================
# Copy- left,right,up,down to anyone that wanna use,modify,delete,ruin it.
# ================================================================================
#
#---------------------------------------------------------------------------------
extractdir="./"

for arg
  do printf '%s\n' "$arg" >> /tmp/unrardirs
done

sort -o /tmp/unrardirs{,}

cat /tmp/unrardirs | while read line
do
  if compgen -G "$line/*.part01.rar" > /dev/null;
  then
    # unrar the file named *.part01.rar..part99.rar
    rar x "$line/*.part01.rar" $extractdir
  elif compgen -G "$line/*.rar" > /dev/null;
  then
    # unrar files named *.rar..r99
    rar x "$line/*.rar" $extractdir
  fi
done

rm /tmp/unrardirs

exit 0
