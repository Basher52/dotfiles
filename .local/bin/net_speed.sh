#!/bin/bash

# This shell script shows the network speed, both received and transmitted.

# Usage: net_speed.sh interface
#   e.g: net_speed.sh eth0


# Global variables
interface=$1
direction=$2
received_bytes=""
old_received_bytes=""
transmitted_bytes=""
old_transmitted_bytes=""

if [ -e $direction ];
then
  exit 0
fi

# This function parses /proc/net/dev file searching for a line containing $interface data.
# Within that line, the first and ninth numbers after ':' are respectively the received and transmited bytes.
get_bytes()
{
    line=$(cat /proc/net/dev | grep $interface | cut -d ':' -f 2 | awk '{print "received_bytes="$1, "transmitted_bytes="$9}')
    eval $line
}


# Function which calculates the speed using actual and old byte number.
# Speed is shown in KByte per second when greater or equal than 1 KByte per second.
# This function should be called each second.
get_velocity()
{
    value=$1
    old_value=$2

    let vel=$value-$old_value
    let velKB=$vel/1024
    let velMB=$velKB/1024
    if [ $velMB != 0 ];
    then
      echo -n "$velMB""M";
    elif [ $velKB != 0 ];
    then
      echo -n "$velKB""K";
    else
      echo -n "$vel""B";
    fi
}

# Gets initial values.
get_bytes
old_received_bytes=$received_bytes
old_transmitted_bytes=$transmitted_bytes

# Shows a message and waits for one second.
#echo "Starting..."
sleep 1
#echo ""


# Main loop. It will repeat forever.
#while true;
#do

    # Get new transmitted and received byte number values.
    get_bytes

    # Calculates speeds.
    vel_recv=$(get_velocity $received_bytes $old_received_bytes)
    vel_trans=$(get_velocity $transmitted_bytes $old_transmitted_bytes)

    # Shows results in the console.
    #echo -en "$interface DOWN:$vel_recv\tUP:$vel_trans\r"
    #echo -en "$vel_recv $vel_trans\r"
    if [ $direction == "up" ];
    then
      echo $vel_trans
    elif [ $direction == "down" ];
    then
      echo $vel_recv
    fi

    # Update old values to perform new calculations.
    old_received_bytes=$received_bytes
    old_transmitted_bytes=$transmitted_bytes

    # Waits one second.
 #   sleep 1;

#done
