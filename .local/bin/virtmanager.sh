#!/bin/env bash

#########################
#
# 230210
#
# A script to start libvirtd if it ain't started already.
# This is 'cos virt-manager can't run without it.
# After that it'll start virt-manager
#
#########################

status=`systemctl status libvirtd|awk -F' ' '/Active/ {print $2}'`
sleep 2

if [ $status == 'inactive' ]; then
  #systemctl start libvirtd && sudo mount -t 9p -o trans=virtio sv sv
  systemctl start libvirtd
fi
virt-manager &

exit 0
