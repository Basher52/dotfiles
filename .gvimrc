" Plugins:
  source /home/basher52/.vimplugins
  "source /home/basher52/.vim_netrw


" Basics:
  let mapleader=" "
  let g:netrw_banner = 0
  set encoding=UTF-8 nocursorline splitbelow splitright wildmode=longest,list,full
  set clipboard=unnamedplus number relativenumber nocompatible
  set shiftwidth=2 autoindent smartindent tabstop=2 softtabstop=2 expandtab
  set fillchars+=eob:\
  set mouse=a
" Removes pipes | that act as separators on splits
"  set fillchars+=vert:\
"  syntax enable

" Statusline
  set nocompatible
  set noruler
  set laststatus=2
  set statusline=
  set statusline+=%#NonText#
  set statusline+=%=
  set statusline+=\ %F
  set statusline+=%#CursorLineNr#
  set statusline+=\ %y
  set statusline+=\ %r
  set statusline+=%#DiffText#
  set statusline+=\ %l/%L
  set statusline+=\ [%c]

" Mouse scrolling
  "set mouse=nicr

" Color Settings
  colorscheme gruvbox
  set background=dark
  set termguicolors
  " Transpareny
"  hi! Normal ctermbg=NONE guibg=NONE
"  hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE


" Set clipboard to be system wide
" Set clipboard=unnamed
" Keybindings
  "nnoremap Q <nop>		Turn off recording
  nnoremap <leader>q :bd<CR>
  "nnoremap <leader><Up> :resize +2<CR>
  "nnoremap <leader><Down> :resize -2<CR>
  "nnoremap <leader><Left> :vertical resize +2<CR>
  "nnoremap <leader><Right> :vertical resize -2<CR>
  nnoremap <leader>s :source ~/.vimrc<CR>
  vnoremap <C-c> "+y
  map <C-v> "+P
  vnoremap <C-c> "*y :let @+=@*<CR>
  nnoremap <C-l> :set foldmethod=indent<CR>
  nnoremap <leader><C-l> :set nofoldenable<CR>
" Spell-check
  map <F6> :setlocal spell! spelllang=en_us<CR>
  map <F7> :setlocal spell! spelllang=sv<CR>
  nnoremap <leader>n :Explore<CR>
  nnoremap <leader>z :set invrnu invnu<CR>
  nnoremap <leader><C-n> :set nofoldenable<CR>
  nnoremap <C-i> :set foldmethod=indent<CR>
  nnoremap <C-s> :set foldmethod=syntax<CR>
  nnoremap <leader><Space> :CtrlP<CR>
  xnoremap K :move '<-2<CR>gv-gv
  xnoremap J :move '>+1<CR>gv-gv

" ShellCheck
 " map <leadeer>s :clear && shellcheck %<CR>

" Remove all trailing whitespace on save.
  autocmd BufWritePre * %s/\s\+$//e

" Kill and Rerun sxhkd to reload changes
  "autocmd BugWritePost *sxhkdrc !killall sxhkd; setsid sxhkd &

" Open file at the place is was when you left it.
  if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
  endif

