#--import the ovpn file into openvpn
#	sudo nmcli connection import type openvpn file .private/OpenVPN\ Bahnhof/client.ovpn
#
#--Show if the import worked
#nmcli connection show

#--Delete an previous import
#	sudo nmcli connection delete type openvpn file .private/OpenVPN\ Bahnhof/client.ovpn
#
#--Open the VPN connection with 'ask for pass'
nmcli connection up Bahnhof --ask

#--Shutdown the VPN connection
#nmcli connection down Bahnhof
#
#--Also a check to see if the connection it UP or DOWN
ip route

sleep 5

### and as always, change the appropriate values for your installation ###
### such as directory, files names ###
