#!/bin/env bash
#
#
# This script asks for the link and then records that to $HOME/Music
#
# 250208
#
#
# ex. Save the file from directory.shoutcst.com in m3u8-format to a specific dir...
# Read that file and copy the 'http....'-line and put it into 'memory'.
# That will be the stream we're copying.
# If suffix of the output file is other than 'opus', like mp3 it'll won't work.
#


ffmpeg -i https://streams.radiomast.io:443/0cef93cd-5974-43b1-868e-c739e81f4f2b -reconnect_at_eof 1 -reconnect_streamed 1 -reconnect_delay_max 3600 -icy 1 -err_detect ignore_err -acodec libopus -cutoff 12000 -ab 32k -compression_level 10 -frame_duration 60 -f segment -segment_time 24:0:0.0 -segment_atclocktime 1 -strftime 1 'ShoutCast_250207_start-1130PM.opus'
§
