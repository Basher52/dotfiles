#!/bin/env bash
#-------------------------------------------
# Closing or opening the VPN connection
#
# Parameters: 1 or 2, 1=open, 2=close
#

type=$1

case $type in
  1)
    echo "Start VPN"
    sudo wg-quick up $HOME/.private/SE-Integrity.conf ;;
  2)
    echo "End VPN"
    sudo wg-quick down $HOME/.private/SE-Integrity.conf ;;
  *)
  echo "Invalid answer" ;;
  esac

  sleep 1
