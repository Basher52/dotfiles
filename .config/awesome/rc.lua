-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.

-- Add a cool looking border with maximize, move, etc with mouse click or mouse right click on the border.
--require('smart_borders'){ show_button_tooltips = true }

pcall(require, "luarocks.loader")
--require('keychord')

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
              require("awful.autofocus")
local lain  = require("lain")
--local async = require("b52widgets")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")
local capslock = require("capslock")

local wibar_widget = {}
wibar_widget.confdir = os.getenv("HOME") .. "/.config/awesome/themes/multicolor"

-- awesome scratchpad
local scratchpad = require("awesome-scratchpad/scratchpad")

wibar_widget.font                                      = "mononoki Nerd Font Mono 12"

wibar_widget.widget_clock                              = wibar_widget.confdir .. "/icons/clock.png"
wibar_widget.widget_uptime                             = wibar_widget.confdir .. "/icons/ac.png"
wibar_widget.widget_cpu                                = wibar_widget.confdir .. "/icons/cpu.png"
wibar_widget.widget_weather                            = wibar_widget.confdir .. "/icons/dish.png"
wibar_widget.widget_mem                                = wibar_widget.confdir .. "/icons/mem.png"
wibar_widget.widget_netdown                            = wibar_widget.confdir .. "/icons/net_down.png"
wibar_widget.widget_temp                               = wibar_widget.confdir .. "/icons/temp.png"
wibar_widget.widget_netup                              = wibar_widget.confdir .. "/icons/net_up.png"
wibar_widget.widget_batt                               = wibar_widget.confdir .. "/icons/bat.png"
wibar_widget.bg_normal                                 = "#000000"
wibar_widget.fg_normal                                 = "#aaaaaa"
wibar_widget.widget_ac                                 = wibar_widget.confdir .. "/icons/ac.png"
wibar_widget.widget_battery                            = wibar_widget.confdir .. "/icons/battery.png"
wibar_widget.widget_battery_low                        = wibar_widget.confdir .. "/icons/battery_low.png"
wibar_widget.widget_battery_empty                      = wibar_widget.confdir .. "/icons/battery_empty.png"

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local np_map = { 87, 88, 89, 83, 84, 85, 79, 80, 81 }
local keynumber = 9

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
--require("awful.hotkeys_popup.keys")

-- #########################################################################################################
-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
-- #########################################################################################################
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "What the fuck did you do?, errors in the config!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oh man! :< :<",
                         text = tostring(err) })
        in_error = false
    end)
end

local my_dropdown = lain.util.quake({
  app = "alacritty",
  argname = '--class %s',
  name = 'myshittydropdown',
  height = 0.5,
  followtag = true,
  visible = false
})

-- }}}
-- #########################################################################################################
beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.old.lua")

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor = os.getenv("EDITOR")
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey  = "Mod4" -- SuperKey
altkey  = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    --awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "edit config", terminal.." vim ~/.config/awesome/rc.lua" },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal },
                                    { "reboot PC", function () os.execute(string.format("reboot")) end }
                                  } })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibar

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t) if client.focus then client.focus:move_to_tag(t) end end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t) if client.focus then client.focus:toggle_tag(t) end end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))


-- fix this later... change function to use xrandr
-- then remove remarks on everything regarding 'wallpaper'
--
--local function set_wallpaper(s)
--    -- Wallpaper
--    if beautiful.wallpaper then
--        local wallpaper = beautiful.wallpaper
--        -- If wallpaper is a function, call it with the screen
--        if type(wallpaper) == "function" then
--            wallpaper = wallpaper(s)
--        end
--        gears.wallpaper.maximized(wallpaper, s, true)
--    end
--end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
--screen.connect_signal("property::geometry", set_wallpaper)

local markup = lain.util.markup
local netdownicon = wibox.widget.imagebox(wibar_widget.widget_netdown)
local netdowninfo = wibox.widget.textbox()







local colortextbox = wibox.widget.textbox()
colortextbox:set_markup(markup.fontfg(wibar_widget.font, "#3388dd", " wwwww "))

local clrwatchbox = awful.widget.watch(markup("#112233", "bash -c '~/.config/awesome/clock'"), 5)
--clrwatchbox:set_markup(markup.fontfg(wibar_widget.font, "#4499ee",5))



local netupicon = wibox.widget.imagebox(wibar_widget.widget_netup)
-- TEXTCLOCK
os.setlocale(os.getenv("LANG")) -- to localize the clock
local clockicon = wibox.widget.imagebox(wibar_widget.widget_clock)
--local mytextclock = wibox.widget.textclock(markup("#7788af", "%y%m%d v%V") .. markup("#535f7a", "") .. markup("#de5e1e", " %H:%M "))
local mytextclock = wibox.widget.textclock(markup("#7788af", "%y%m%d v%V") .. markup("#de5e1e", " %H:%M "))
mytextclock.font = wibar_widget.font





-- CALENDAR
wibar_widget.cal = lain.widget.cal({
    attach_to = { mytextclock },
    notification_preset = {
        fg   = wibar_widget.fg_normal,
        bg   = wibar_widget.bg_normal
    }
})
-- NET
local netupinfo = lain.widget.net({
    settings = function()
--        if iface ~= "network off" and
--           string.match(wibar_widget.weather.widget.text, "N/A")
--        then
--            wibar_widget.weather.update()
--        end

        widget:set_markup(markup.fontfg(wibar_widget.font, "#e54c62", net_now.sent .. " "))
        netdowninfo:set_markup(markup.fontfg(wibar_widget.font, "#87af5f", net_now.received .. " "))
    end
})

-- MEM
local memicon = wibox.widget.imagebox(wibar_widget.widget_mem)


--awful.widget.watch('bash -c "~/.local/bin/up-time"', 60, function(widget, stdout) widget:set_markup(stdout) end),

local memory = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(wibar_widget.font, "#e0da37", mem_now.used .. "M "))
    end
})

-- CORETEMP
local tempicon = wibox.widget.imagebox(wibar_widget.widget_temp)
local temp = lain.widget.temp({
    settings = function()
        widget:set_markup(markup.fontfg(wibar_widget.font, "#f1af5f", coretemp_now .. "°C "))
    end
})

-- CPU
local cpuicon = wibox.widget.imagebox(wibar_widget.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(wibar_widget.font, "#e33a6e", cpu_now.usage .. "% "))
    end
})

-- WEATHER
local weathericon = wibox.widget.imagebox(wibar_widget.widget_weather)
wibar_widget.weather = lain.widget.weather({
    city_id = 2720383, -- placeholder (Sweden)
    APPID = "1a9b50eacfa333d6c82cab6d8739c864",
    notification_preset = { font = "mononoki Nerd Font Mono 14", fg = wibar_widget.fg_normal },
    weather_na_markup = markup.fontfg(wibar_widget.font, "#eca4c4", "N/A "),
    settings = function()
        descr = weather_now["weather"][1]["description"]:upper()
        units = math.floor(weather_now["main"]["temp"])
        widget:set_markup(markup.fontfg(wibar_widget.font, "#eca4c4", descr .. " " ..  units .. "°C "))
    end
})


-- BATTERY
local baticon = wibox.widget.imagebox(wibar_widget.widget_batt)
local bat = lain.widget.bat({
    settings = function()
        if bat_now.status and bat_now.status ~= "N/A" then
            if bat_now.ac_status == 1 then
                widget:set_markup(markup.font(theme.font, " AC "))
                baticon:set_image(wibar_widget.widget_ac)
                return
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 5 then
                baticon:set_image(wibar_widget.widget_battery_empty)
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 15 then
                baticon:set_image(wibar_widget.widget_battery_low)
            else
                baticon:set_image(wibar_widget.widget_battery)
            end
            widget:set_markup(markup.font(wibar_widget.font, " " .. bat_now.perc .. "% "))
        else
            widget:set_markup()
            baticon:set_image(wibar_widget.widget_ac)
        end
    end
})

local quake = require("util/quake")
awful.screen.connect_for_each_screen(function(s)
     -- Wallpaper
    --set_wallpaper(s)
    s.quake = quake({ app = "alacritty",argname = "--title %s",extra = "--class QuakeDD -e tmux", visible = true, height = 0.9, screen = s })

    -- Each screen has its own tag table.
   s.mypromptbox = awful.widget.prompt()

   --awful.tag({ " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9", " ff ", " br ", " chr ", " qute" }, s, awful.layout.layouts[1])
   awful.tag({ " ➊ ", " ➋ ", " ➌ ", " ➍ ", " ➎ ", " ➏ ", " ➐ ", " ➑ ", " ➒ ", " FF ", " BR ", " CHR ", " QUTE" }, s, awful.layout.suit.tile)

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all
	,buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

        -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = 18 })
--    s.mytray = awful.wibar({ position = "bottom", screen = s, height = 18 })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            wibox.widget.systray()
        },
            s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,

            --awful.widget.watch('bash -c "~/.local/bin/somethingtoshow"', 3600),
            awful.widget.watch('bash -c "~/.local/bin/bt_headphone_batterycheck.sh"', 100, function(widget, stdout) widget:set_markup(stdout) end),
            netdownicon, netdowninfo,
            netupicon, netupinfo.widget,
            memicon, memory.widget,
            cpuicon, cpu.widget,
            baticon, bat.widget,
            clrwatchbox,
            weathericon, awful.widget.watch('bash -c "~/.local/bin/ansi-weather.sh"', 600, function(widget, stdout) widget:set_markup(stdout) end),

            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/up-time"', 60, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/bgit_commits"', 180, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/updates"', 120, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/nvidia-fanspeed.sh"', 30, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/temperature"', 5, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/ipaddr.sh"', 3600, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
            awful.widget.watch('bash -c "~/.local/bin/diskfree.sh"', 120, function(widget, stdout) widget:set_markup(stdout) end),
            wibox.widget.textbox(' | '),
--          awful.widget.watch("bash -c '~/.local/bin/clock'", 5),
            capslock,
                -- more config here
            clockicon,
            mytextclock,
            s.mylayoutbox
        },
    }
end)
-- }}}

-- #################################################################################################
-- {{{ Mouse bindings
-- #################################################################################################
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end), --mod+L-Click=Move to other position.
    awful.button({ }, 4, awful.tag.viewnext), -- ??, don't know what mouse-key3 is.
    awful.button({ }, 5, awful.tag.viewprev)  -- mod+R-Click=Resize all existing tiles at the same time or selected floating tile.
))
-- }}}

-- #################################################################################################
-- {{{ KEY BINDINGS
-- #################################################################################################
globalkeys = gears.table.join(

    -- {{{ Personal keybindings

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
  capslock.key,
    -- dmenu
    awful.key({ modkey, "Shift" }, "Return", function () awful.spawn(string.format("dmenu_run", beautiful.bg_normal, beautiful.fg_normal, beautiful.bg_focus, beautiful.fg_focus)) end,
        {description = "show dmenu", group = "hotkeys"}),

    -- dmenu scripts (Alt+Ctrl+Key)
    awful.key({ altkey, "Control" }, "e", function () awful.util.spawn( "./.dmenu/dmenu-edit-configs.sh" ) end, {description = "edit config files" , group = "dmenu scripts" }),
    awful.key({ altkey, "Control" }, "m", function () awful.util.spawn( "./.dmenu/dmenu-sysmon.sh" ) end, {description = "system monitoring apps" , group = "dmenu scripts" }),
    awful.key({ altkey, "Control" }, "p", function () awful.util.spawn( "passmenu" ) end, {description = "passmenu" , group = "dmenu scripts" }),
    awful.key({ altkey, "Control" }, "s", function () awful.util.spawn( "./.dmenu/dmenu-surfraw.sh" ) end, {description = "surfraw web search" , group = "dmenu scripts" }),
    awful.key({ altkey, "Control" }, "t", function () awful.util.spawn( "./.dmenu/dmenu-trading.sh" ) end, {description = "trading programs" , group = "dmenu scripts" }),

    -- CLI applications (Super+Alt+Key)
    awful.key({ modkey, altkey    }, "space", function () awful.util.spawn( terminal.." -e ./.config/awesome/opensshconnection" ) end, {description = "open ssh on FTP" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "0", function () awful.util.spawn( terminal.." -e ./.config/awesome/bgit.sh" ) end, {description = "bgit script" , group = "git" }),
    awful.key({ modkey, altkey    }, "1", function () awful.util.spawn( terminal.." -e pacu" ) end, {description = "pacman update all" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "2", function () awful.util.spawn( terminal.." -e yayu" ) end, {description = "yay update all" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "3", function () awful.util.spawn( terminal.." -e ssh 192.168.68.250" ) end, {description = "ssh FTP-Server" , group = "FTP" }),
    awful.key({ modkey, altkey    }, "5", function () awful.util.spawn( terminal.." -e ncftp -u basher52 -P 1138 192.168.68.250" ) end, {description = "FTP" , group = "FTP" }),
    awful.key({ modkey, altkey    }, "a", function () awful.util.spawn( terminal.." -e ncpamixer" ) end, {description = "ncpamixer" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "b", function () awful.util.spawn( "brave" ) end, {description = "brave web browser" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "c", function () awful.util.spawn( terminal.." -e cmus" ) end, {description = "cmus" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "d", function () awful.util.spawn( terminal.." -e ncdu" ) end, {description = "space usage" , group = "terminal apps" }),
    awful.key({ modkey, altkey, "Shift"}, "d", function () awful.util.spawn( "discord" ) end, {description = "discord" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "f", function () awful.util.spawn( "firefox" ) end, {description = "firefox web browser" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "h", function () awful.util.spawn( "chromium" ) end, {description = "chromium web browser" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "i", function () awful.util.spawn( terminal.." -e irssi" ) end, {description = "irssi" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "l", function () awful.util.spawn( terminal.." -e lynx --cfg=~/.lynx/lynx.cfg --lss=~/.lynx/lynx.lss -vikeys gopher://distro.tube" ) end, {description = "lynx cli browser" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "o", function () awful.util.spawn( "floorp" ) end, {description = "floorp web browser" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "m", function () awful.util.spawn( "mpv --playlist=/tmp/mpv-playlist" ) end, {description = "Start MPV using playlist" , group = "gui apps" }),
    --awful.key({ modkey, altkey    }, "m", function () awful.util.spawn( terminal.." -e toot curses" ) end, {description = "toot curses" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "n", function () awful.util.spawn( terminal.." -e newsboat" ) end, {description = "newsboat" , group = "terminal apps" }), -- RSS CLI Reader
    awful.key({ modkey, altkey    }, "p", function () awful.util.spawn( "/usr/bin/wine /home/basher52/.wine/drive_c/Programs/FlashFXP/FlashFXP.exe" ) end, {description = "newsboat" , group = "terminal apps" }), -- RSS CLI Reader
    --awful.key({ modkey, altkey    }, "p", function () awful.util.spawn( terminal.." -e /usr/bin/wine /home/basher52/.wine/drive_c/Programs/FlashFXP/FlashFXP.exe" ) end, {description = "newsboat" , group = "terminal apps" }), -- RSS CLI Reader
    awful.key({ modkey, altkey    }, "q", function () awful.util.spawn( "qutebrowser" ) end, {description = "qutebrowser" , group = "gui apps" }),
    --awful.key({ modkey, altkey    }, "r", function () awful.util.spawn( "virt-manager" ) end, {description = "virt-manager" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "r", function () awful.util.spawn( terminal.." -e ./.config/awesome/virtmanager.sh" ) end, {description = "virt-manager" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "s", function () awful.util.spawn( terminal.." -e castero" ) end, {description = "castero - PodPlayer", group = "terminal apps" }),
    --awful.key({ modkey, altkey    }, "s", function () awful.util.spawn( "surf www.youtube.com/c/DistroTube" ) end, {description = "surf web browser" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "t", function () awful.util.spawn( "birdtray" ) end, {description = "thunderbird mail client" , group = "gui apps" }), -- Starts with birdtray
    awful.key({ modkey, altkey,   }, "v", function () awful.util.spawn( terminal.. " -e ./.config/awesome/vpn.sh 1" ) end, {description = "open and check VPN" , group = "terminal apps" }),
    awful.key({ modkey, altkey, "Shift"}, "v", function () awful.util.spawn( terminal.. " -e ./.config/awesome/vpn.sh 2" ) end, {description = "close and check VPN" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "w", function () awful.util.spawn( "waterfox" ) end, {description = "waterfox web browser" , group = "gui apps" }),
    awful.key({ modkey, altkey    }, "x", function () awful.util.spawn( terminal.." -e tuxi" ) end, {description = "tuxi - search google thru CLI" , group = "terminal apps" }),
    awful.key({ modkey, altkey    }, "y", function () awful.util.spawn( terminal.." -e youtube-viewer" ) end, {description = "youtube-viewer" , group = "terminal apps" }), -- CLI YT-Viewer
    awful.key({ modkey, altkey    }, "z", function () awful.util.spawn( "pcmanfm" ) end, {description = "pcmanfm file manager" , group = "gui apps" }),

-- screenshots
    awful.key({ }, "Print", function () awful.util.spawn("scrot 'ArchLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'") end, {description = "Scrot", group = "screenshots"}),



    awful.key({ modkey,    }, "S", function () awful.util.spawn( "ncdu" ) end, {description = "space usage" , group = "terminal apps" }),




    -- Personal keybindings}}}

    -- Hotkeys Awesome
    awful.key({ modkey,        }, "s",hotkeys_popup.show_help, {description = "show help", group="awesome"}),

    -- Tag browsing with modkey
    awful.key({ modkey,        }, "Left", awful.tag.viewprev, {description = "view previous", group = "tag"}),
    awful.key({ modkey,        }, "Right", awful.tag.viewnext, {description = "view next", group = "tag"}),
    awful.key({ modkey, altkey }, "Escape", awful.tag.history.restore, {description = "go back", group = "tag"}),

    -- Default client focus
    awful.key({ modkey, altkey }, "Right", function () awful.client.focus.byidx( 1) end, {description = "focus next by index", group = "client"}),
    awful.key({ modkey, altkey }, "Left", function () awful.client.focus.byidx(-1) end, {description = "focus previous by index", group = "client"}),

    -- By direction client focus
    awful.key({ modkey }, "j", function() awful.client.focus.global_bydirection("down") if client.focus then client.focus:raise() end end, {description = "focus down", group = "client"}),
    awful.key({ modkey }, "k", function() awful.client.focus.global_bydirection("up") if client.focus then client.focus:raise() end end, {description = "focus up", group = "client"}),
    awful.key({ modkey }, "h", function() awful.client.focus.global_bydirection("left") if client.focus then client.focus:raise() end end, {description = "focus left", group = "client"}),
    awful.key({ modkey }, "l", function() awful.client.focus.global_bydirection("right") if client.focus then client.focus:raise() end end, {description = "focus right", group = "client"}),

    -- By direction client focus with arrows
    awful.key({ "Control", modkey }, "Down", function() awful.client.focus.global_bydirection("down") if client.focus then client.focus:raise() end end, {description = "focus down", group = "client"}),
    awful.key({ "Control", modkey }, "Up", function() awful.client.focus.global_bydirection("up") if client.focus then client.focus:raise() end end, {description = "focus up", group = "client"}),
    awful.key({ "Control", modkey }, "Left", function() awful.client.focus.global_bydirection("left") if client.focus then client.focus:raise() end end, {description = "focus left", group = "client"}),
    awful.key({ "Control", modkey }, "Right", function() awful.client.focus.global_bydirection("right") if client.focus then client.focus:raise() end end, {description = "focus right", group = "client"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1) end, {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1) end, {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey            }, ".", function () awful.screen.focus_relative( 1) end, {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey            }, ",", function () awful.screen.focus_relative(-1) end, {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto, {description = "jump to urgent client", group = "client"}),

    -- Show/Hide Wibox
    awful.key({ modkey }, "b", function () for s in screen do s.mywibox.visible = not s.mywibox.visible if s.mybottomwibox then s.mybottomwibox.visible = not s.mybottomwibox.visible end end end,
        {description = "toggle wibox", group = "awesome"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn( terminal.. " -e bash" ) end, {description = "alacritty with bash shell", group = "super"}),
    awful.key({ modkey, altkey    }, "Return", function () awful.spawn( "kitty -e bash" ) end, {description = "kitty with bash shell", group = "super"}),
    awful.key({ modkey, "Shift"   }, "r", awesome.restart, {description = "reload awesome", group = "awesome"}),
    awful.key({ altkey, "Control" }, "r", function () awful.util.spawn( terminal.." -e vim ./.config/awesome/rc.lua" ) end, {description = "Edit rc.lua" , group = "awesome" }),

    awful.key({ modkey, "Shift"   }, "Right", function () awful.tag.incmwfact( 0.01) end, {description = "increase master width", group = "Client layout"}),
    awful.key({ modkey, "Shift"   }, "Left",  function () awful.tag.incmwfact(-0.01) end, {description = "decrease master width", group = "Client layout"}),
    awful.key({ modkey, "Shift"   }, "Up",    function () awful.client.incwfact( 0.01) end, {description = "increase selected client height", group = "Client layout"}),
    awful.key({ modkey, "Shift"   }, "Down",  function () awful.client.incwfact(-0.01) end, {description = "decrease selected client height", group = "Client layout"}),

    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end, {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end, {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true) end, {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true) end, {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1) end, {description = "select next window layout", group = "layout"}),

    awful.key({ modkey, "Control" }, "n", function () local c = awful.client.restore() if c then client.focus = c c:raise() end end, {description = "restore minimized", group = "client"}),

    -- Dropdown application
    --awful.key({ modkey, }, "z", nil, function () my_dropdown().quake:toggle() end, {description = "dropdown application", group = "super"}),
    awful.key({ modkey, }, "z", function () awful.screen.focused().quake:toggle() end, {description = "dropdown application", group = "launcher"}),


    -- On the fly useless gaps resize
    awful.key({ altkey, "Control" }, "j", function() lain.util.useless_gaps_resize(1) end, {description = "increment useless gaps", group = "tag" }),
    awful.key({ altkey, "Control" }, "h", function() lain.util.useless_gaps_resize(-1) end, {description = "decrement useless gaps", group = "tag" }),

    -- System:Hibernate, Shutdown, Restart & awesomewm quit.
    awful.key({ modkey, "Control", "Shift" }, "h", function () os.execute(string.format("systemctl hibernate")) end, {description = "hibernate", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "r", function () os.execute(string.format("reboot")) end, {description = "restart", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "s", function () os.execute(string.format("poweroff")) end, {description = "shutdown", group = "system"}),
    awful.key({ modkey, "Control", "Shift" }, "x", awesome.quit, {description = "quit awesome", group = "system"}),
    -- Awesomewm:'copy rc.lua to default
    awful.key({ modkey, "Control", "Shift" }, "c", function () awful.util.spawn( terminal.." -e ./.config/awesome/rc.lua.cp2original.sh" ) end, {description = "copy rc.lua to replace original" , group = "awesome" }),

    -- Brightness
    awful.key({ }, "XF86MonBrightnessUp", function () os.execute("xbacklight -inc 10") end, {description = "+10%", group = "hotkeys"}),
    awful.key({ }, "XF86MonBrightnessDown", function () os.execute("xbacklight -dec 10") end, {description = "-10%", group = "hotkeys"}),

    -- ALSA volume control
    awful.key({ }, "XF86AudioRaiseVolume", function () os.execute(string.format("amixer set 'Master' 1%%+")) end),
    awful.key({ }, "XF86AudioLowerVolume", function () os.execute(string.format("amixer -q set 'Master' 1%%-")) end),
    awful.key({ }, "XF86AudioMute", function () os.execute(string.format("amixer -q set 'Master' toggle")) end),

    -- Copy primary to clipboard (terminals to gtk)
    awful.key({ modkey }, "c", function () awful.spawn.with_shell("xsel | xsel -i -b") end, {description = "copy terminal to gtk", group = "hotkeys"}),
    -- Copy clipboard to primary (gtk to terminals)
    awful.key({ modkey }, "v", function () awful.spawn.with_shell("xsel -b | xsel") end, {description = "copy gtk to terminal", group = "hotkeys"})


    -- Send or take back client from scratchpad
--    awful.key({ modkey,           }, "0", scratchpad.toggle_send,
--        {keygroup = "numpad", description = "Send to scratch pad", group = "Scratchpad"}),
    -- Toggle the last scratchpad on and off
--    awful.key({ modkey,           }, "7", scratchpad.toggle,
--        {keygroup = "numpad", description = "Toggle Scratch pad", group = "Scratchpad"}),
    -- Cycle through all the avaialable scratchpads
--    awful.key({ modkey,           }, "9", scratchpad.cycle,
--        {keygroup = "numpad", description = "Cycle Scratch pad", group = "Scratchpad"})


)

-- Numpad: [0-9] = [#90, #87-#89, #83-#85, #79-#81]
--for i = 1, keynumber do
--   globalkeys = awful.util.table.join(
--      globalkeys,
--      awful.key({ modkey }, "#" .. np_map[i],
----        function ()
----           local screen = mouse.screen
----           if tags[screen][i] then
--             if np_map[i] == 1 then
--              scratchpad.toggle_send(),
--             end)
----        end),
--      awful.key({ modkey, "Control" }, "#" .. np_map[i],
----        function ()
----           local screen = mouse.screen
----           if tags[screen][i] then
--              scratchpad.toggle()),
----           end
----        end),
--      awful.key({ modkey, "Shift" }, "#" .. np_map[i],
----        function ()
----           if client.focus and tags[client.focus.screen][i] then
--              scratchpad.cycle()))
----           end
----        end),
--end


-- #########################################################################################################

-- #########################################################################################################
clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f", function (c)
	    local tmpSticky = false
	    local tmpOntop = false
	    if c.sticky then
		    tmpSticky = true
	    end
	    if c.omtop then
		    tmpOntop = true
	    end
	    c.fullscreen = not c.fullscreen c:raise()
	    c.sticky = tmpSticky
	    c.ontop = tmpOntop
    end,
	    {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill() end, {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle, {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end, {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen() end, {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop end, {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "y",      function (c) c.sticky = not c.sticky end, {description = "toggle keep sticky (view in all tags)", group = "client"}),

    -- The client currently has the input focus, so it cannot be
    -- minimized, since minimized clients can't have the focus.
    awful.key({ modkey,           }, "n", function (c) c.minimized = true end, {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m", function (c) c.maximized = not c.maximized c:raise() end, {description = "Toggle maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m", function (c) c.maximized_vertical = not c.maximized_vertical c:raise() end, {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m", function (c) c.maximized_horizontal = not c.maximized_horizontal c:raise() end, {description = "(un)maximize horizontally", group = "client"}),

    -- Virtual "echo" commands
    awful.key({ modkey, "Control" }, "z", function () awful.util.spawn( terminal.."echo 'mount -t 9p -o trans=virtio /sharepoint sharefolder'" ) end, {description = "bgit status" , group = "virt-manager" })
)
-- #########################################################################################################

-- #########################################################################################################
-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
-- #########################################################################################################
for i = 1, 13 do
    -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
    local descr_view, descr_toggle, descr_move, descr_toggle_focus
    if i == 1 or i == 9 then
        descr_view = {description = "view tag #", group = "tag"}
        descr_toggle = {description = "toggle tag #", group = "tag"}
        descr_move = {description = "move focused client to tag #", group = "tag"}
        descr_toggle_focus = {description = "toggle focused client on tag #", group = "tag"}
    end
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end



-- #########################################################################################################

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { -- border_width = beautiful.border_width,
      		     --border_width = 1,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

}
-- }}}



-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
--client.connect_signal("mouse::enter", function(c)
--    c:emit_signal("request::activate", "mouse_enter", {raise = false})
--end)

--border_color = "#ff000088",
--client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("focus", function(c) c.border_color = "#ffffff" end)
client.connect_signal("unfocus", function(c) c.border_color = "#000000" c.border_width=1 end)
--client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Start programs at GUI start
awful.spawn.with_shell("killall lxsession ; lxsession &")
--awful.spawn.with_shell("picom --experimental-backends --xrender-sync-fence &")
--awful.spawn.with_shell("picom --experimental-backends &")
--awful.spawn.with_shell("picom --experimental-backends --backend glx &")
awful.spawn.with_shell("picom --xrender-sync-fence &")
awful.spawn.with_shell("setxkbmap se &")
awful.spawn.with_shell("nm-applet &")
awful.spawn.with_shell("killall volumeicon ; volumeicon &")


awful.spawn.with_shell("killall udiskie ; udiskie &")
awful.spawn.with_shell("killall imwheel ; imwheel &")
--awful.spawn.with_shell("nvidia-settings -a GPUFanControlState=1 -a GPUTargetFanSpeed=49")
awful.spawn.with_shell("xrandr -r 144 --dpi 86")
awful.spawn.with_shell("xrandr --output DP-0 --set TearFree on")
awful.spawn.with_shell("nitrogen --restore &")
awful.spawn.with_shell("killall ulauncher ; ulauncher &")
awful.spawn.with_shell("mpv --speed=1 ./.config/autostart/Dont_fail_me_again.mp3 &")
awful.spawn.with_shell("~/.config/awesome/setscale 1.4")
awful.spawn.with_shell("sudo ~/.local/bin/rootgrp.sh")

