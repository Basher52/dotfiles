#!/bin/env bash
#---------------------------------------------------------------------------------
#
# Script to list my Philips Headphones battery status
# Created by Oerjan 'Aliens' Snell.
#
# 231224  Script created.
#
# ================================================================================
# Copy- left,right,up,down to anyone that wanna use,modify,delete,ruin it.
# ================================================================================
#
#---------------------------------------------------------------------------------
batlevel=$(bluetoothctl info $(bluetoothctl devices | grep 'Philips PH805BK' | awk '{print $2}') | awk '/Battery/ {print $4}' | sed s/\(// | sed s/\)//)

if [[ $batlevel -gt 99 ]]
then
  color="#00ff00"
elif [[ $batlevel -gt 79 ]]
then
  color="#aaff00"
elif [[ $batlevel -gt 59 ]]
then
  color="#ccff00"
elif [[ $batlevel -gt 39 ]]
then
  color="#ffaa00"
elif [[ $batlevel -gt 29 ]]
then
  color="#ff7700"
elif [[ $batlevel -gt 14 ]]
then
  color="#ff5000"
else
  color="#ff0000"
fi

if [[ -n "$batlevel" ]]; then
  printf ' \xF0\x9F\x8E\xA7'; printf "<b><span color='$color'>"; echo "$batlevel</span></b>"
fi

exit 0
